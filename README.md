# ISLRnotes

These are notes I took while working through ISLR.

Chapters:

- [x] Statistical learning
- [x] Linear regression
- [ ] Classification
- [ ] Resampling methods
- [ ] Linear Model selection and Regularisation
- [ ] Moving beyond linearity
- [ ] Tree based methods
- [ ] Support vector machines
- [ ] Unsupervised learning


